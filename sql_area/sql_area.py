from PySide2 import QtWidgets
from PySide2.QtGui import QIcon
import mysql.connector
from sql_workspace import config
from sql_area.sql_insert_dialog import InsertDataDialog
import json
from PyQt5.QtWidgets import QMessageBox
from unidecode import unidecode

class SqlArea(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        layout = QtWidgets.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        self.toolBar = QtWidgets.QToolBar()

        self.up = QtWidgets.QAction(QIcon("resources/icons/up.png"),"Up", layout)
        self.down = QtWidgets.QAction(QIcon("resources/icons/down.png"),"Down", layout)
        self.top = QtWidgets.QAction(QIcon("resources/icons/top.png"),"First", layout)
        self.bottom = QtWidgets.QAction(QIcon("resources/icons/bottom.png"),"Last", layout)
        self.delete = QtWidgets.QAction(QIcon("resources/icons/kanta.png"),"Delete", layout)
        self.insert = QtWidgets.QAction(QIcon("resources/icons/new-page.png"),"New Page", layout)
        self.update_entity = QtWidgets.QAction(QIcon("resources/icons/save.png"),"Update entities", layout)

        self.toolBar.addAction(self.up)
        self.toolBar.addAction(self.down)
        self.toolBar.addAction(self.top)
        self.toolBar.addAction(self.bottom)
        self.toolBar.addAction(self.delete)
        self.toolBar.addAction(self.insert)
        self.toolBar.addAction(self.update_entity)

        layout.addWidget(self.toolBar)
        
        self.current_database = ""
        self.current_table = ""
        
        self.toolBarChild = QtWidgets.QToolBar()

        self.upChild = QtWidgets.QAction(QIcon("resources/icons/up.png"),"Up", layout)
        self.downChild = QtWidgets.QAction(QIcon("resources/icons/down.png"),"Down", layout)
        self.topChild = QtWidgets.QAction(QIcon("resources/icons/top.png"),"First", layout)
        self.bottomChild = QtWidgets.QAction(QIcon("resources/icons/bottom.png"),"Last", layout)
        self.deleteChild = QtWidgets.QAction(QIcon("resources/icons/kanta.png"),"Delete", layout)
        self.insertChild = QtWidgets.QAction(QIcon("resources/icons/new-page.png"),"New Page", layout)

        self.toolBarChild.addAction(self.upChild)
        self.toolBarChild.addAction(self.downChild)
        self.toolBarChild.addAction(self.topChild)
        self.toolBarChild.addAction(self.bottomChild)
        self.toolBarChild.addAction(self.deleteChild)
        self.toolBarChild.addAction(self.insertChild)

        layout.addWidget(self.toolBarChild)

        self.tab_widget1 = QtWidgets.QTabWidget()
        self.tab_widget1.setTabsClosable(True)
        self.tab_widget1.tabCloseRequested.connect(self.closeTab)
        layout.addWidget(self.tab_widget1)

        self.updated_items = []

        layout.addWidget(self.toolBarChild)

        self.tab_widget2 = QtWidgets.QTabWidget()

        
        
        layout.addWidget(self.tab_widget2)
        self.tab_widget1.currentChanged.connect(self.tabChanged)
        self.insert.triggered.connect(self.insertData)
        self.delete.triggered.connect(self.deleteData)  
        self.down.triggered.connect(self.moveSelectionDown)
        self.up.triggered.connect(self.moveSelectionUp)
        self.bottom.triggered.connect(self.moveSelectionBottom)
        self.top.triggered.connect(self.moveSelectionTop)
        self.update_entity.triggered.connect(self.update_entities)


        self.insertChild.triggered.connect(self.insertChildData)
        self.deleteChild.triggered.connect(self.deleteChildData)  
        self.downChild.triggered.connect(self.moveChildSelectionDown)
        self.upChild.triggered.connect(self.moveChildSelectionUp)
        self.bottomChild.triggered.connect(self.moveChildSelectionBottom)
        self.topChild.triggered.connect(self.moveChildSelectionTop)


    def onClicked(self, table, database):
        tab_names = []
        for index in range(self.tab_widget1.count()):
            tab_name = self.tab_widget1.tabText(index)
            tab_names.append(tab_name)

        

        datatable = database + "/" + table
        if datatable not in tab_names:

            self.database = database
            self.table = table
            myconfig = config.load_config("myconfig.json")

            connection = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db=database
            )
            cursor = connection.cursor(buffered=True)
            query = "SELECT * FROM " + table
            cursor.execute(query)
            table_data = cursor.fetchall()
            column_names = [desc[0] for desc in cursor.description]



            query = "SELECT ime_tabele FROM metapodaci WHERE kodno_ime_tabele = %s"
            cursor.execute(query, (table,))

            result = cursor.fetchone()
            cursor.close()
            connection.close()
            table_widget = QtWidgets.QTableWidget(self)
            table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
            data = table_data
            

            table_widget.setColumnCount(len(column_names))
            table_widget.setHorizontalHeaderLabels(column_names)
            table_widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)  

            table_widget.setRowCount(len(data))
            for row_index, row_data in enumerate(data):
                for col_index, cell_value in enumerate(row_data):
                    item = QtWidgets.QTableWidgetItem(str(cell_value))
                    table_widget.setItem(row_index, col_index, item)

            table_widget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

        table_widget.selectRow(0)
        table_widget.itemSelectionChanged.connect(self.item_selected)
        table_widget.itemSelectionChanged.connect(self.tabChanged)
        self.tab_widget1.addTab(table_widget, database + "/" + result[0])

        self.tab_widget1.setCurrentWidget(table_widget)
        
            
    def item_selected(self):
        current_widget  = self.tab_widget1.currentWidget()
        selected_items  = current_widget.selectedItems()
        if selected_items :
            selected_item = selected_items [0]  
            row = selected_item.row()
            self.updated_items.append(row)
            
    def get_primary_key_column_name(self):
        query = f"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = '{self.current_table}' AND CONSTRAINT_NAME = 'PRIMARY'"
        myconfig = config.load_config("myconfig.json")
        connection = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=self.current_database
        )
        cursor = connection.cursor(buffered=True)
        cursor.execute(query)
        results = cursor.fetchall()
        connection.close()

        if results is not None:
            return [result[0] for result in results]
        
        return None
    
    def update_entities(self):
        current_widget = self.tab_widget1.currentWidget()
        items = []
        if len(self.updated_items) > 0:
            column_count = current_widget.columnCount()
            column_names = []
            items_rows = []
            
            for column in range(column_count):
                header_item = current_widget.horizontalHeaderItem(column)
                if header_item is not None:
                    column_names.append(header_item.text())

            for row in self.updated_items:
                item_values = {}
                items_rows.append(row)
                column_counter = 0
                for column in range(column_count):
                    item = current_widget.item(row, column)
                    if item is not None:
                        item_values[column_names[column_counter]] = item.text()
                        column_counter += 1

                items.append(item_values)
        
        if len(items) > 0:
            try:
                myconfig = config.load_config("myconfig.json")
                position_counter = 0
                primary_key_columns = self.get_primary_key_column_name()

                for json_object in items:
                    position = items_rows[position_counter]
                    position_counter += 1
                    connection = mysql.connector.connect(
                        host=myconfig["host"],
                        user=myconfig["user"],
                        password=myconfig["password"],
                        db=self.current_database
                    )
                    cursor = connection.cursor(buffered=True)

                    # Get primary key values for the row at the desired position
                    primary_key_values = []
                    for primary_key_column in primary_key_columns:
                        select_query = f"SELECT {primary_key_column} FROM {self.current_table} LIMIT 1 OFFSET {position}"
                        cursor.execute(select_query)
                        result = cursor.fetchone()
                        primary_key_values.append(result[0])

                    update_query = f"UPDATE {self.current_table} SET "

                    set_clauses = []
                    values = []
                    for column, value in json_object.items():
                        set_clause = f"{column} = %s"
                        set_clauses.append(set_clause)
                        values.append(value)

                    update_query += ", ".join(set_clauses)

                    where_clauses = []
                    for primary_key_column, primary_key_value in zip(primary_key_columns, primary_key_values):
                        where_clause = f"{primary_key_column} = %s"
                        where_clauses.append(where_clause)
                        values.append(primary_key_value)

                    where_query = " AND ".join(where_clauses)
                    if len(where_clauses) != 0:
                        update_query += f" WHERE {where_query}"

                    # Execute the query
                    cursor.execute(update_query, values)
                    connection.commit()

                    # Close the database connection
                    cursor.close()
                    connection.close()
            except Exception as e:
                message_box = QMessageBox()
                message_box.setWindowTitle("Error")
                message_box.setText(str(e))
                message_box.setIcon(QMessageBox.Information)
                message_box.setStandardButtons(QMessageBox.Ok)
                message_box.exec_()
                connection.close()
                self.updated_items = []
                return
                
            message_box = QMessageBox()
            message_box.setWindowTitle("Information")
            message_box.setText("Sve vrednosti su uspesno promenjene.")
            message_box.setIcon(QMessageBox.Information)
            message_box.setStandardButtons(QMessageBox.Ok)
            message_box.exec_()
        else:
            message_box = QMessageBox()
            message_box.setWindowTitle("Information")
            message_box.setText("Vrednost ni jedne kolone nije promenjena tako da nije moguce izvrsiti update entiteta.")
            message_box.setIcon(QMessageBox.Information)
            message_box.setStandardButtons(QMessageBox.Ok)
            message_box.exec_()

        self.updated_items = []    
    
    
    def close_table_tab_by_name(self, table_name):
        self.updated_items = []
        for index in range(self.tab_widget1.count()):
            tab_name = self.tab_widget1.tabText(index)
            if tab_name == table_name:
                self.tab_widget1.removeTab(index)
                
    #TODO: Kada se zatvori tab baze, teba zatvoriti sve tabove tabela iz te baze 
    def close_all_table_tabs_by_database_name(self, database_name):
        self.updated_items = []
        for index in range(self.tab_widget1.count() - 1, -1, -1):
            tab_name = self.tab_widget1.tabText(index)
            if database_name in tab_name:
                self.tab_widget1.removeTab(index)
           

    def tabChanged(self):
        self.tab_widget2.clear()
        string = self.tab_widget1.tabText(self.tab_widget1.currentIndex())
        database, t = string.split('/')
        self.current_database = database

        table = t.lower().replace(" ", "_")
        self.current_table = unidecode(table)
        
        myconfig = config.load_config("myconfig.json")
        connection = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=database
        )
        cursor = connection.cursor()
        query = "SELECT VezTKod FROM vezanetabele WHERE OsnTKodIme = %s"
        cursor.execute(query, (table,))
        connected_tables = cursor.fetchall()


        cursor.close()
        connection.close()

        for connected_table in connected_tables:
            connected_table_name = connected_table[0]
            self.openTable(table, connected_table_name, self.database, self.tab_widget2)






    def openTable(self, parent_table, table, database, tab_widget):
        tab_names = [tab_widget.tabText(index) for index in range(tab_widget.count())]
        myconfig = config.load_config("myconfig.json")
        connection = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db=database
            )
        cursor = connection.cursor(buffered=True)
        table_widget2 = self.tab_widget1.currentWidget()
        item = None
        for i in table_widget2.selectedItems():
            item = i.text()            
            break
        
        column_name = self.getColumnFromSelectedItem(table_widget2)
        query = "SELECT VezaneKod from vezanetabele WHERE OsnTKodIme = %s AND VezTKod = %s"
        cursor.execute(query, (parent_table, table,))
        vezanekod = cursor.fetchone()


        query = "SELECT OsnvKod from vezanetabele WHERE OsnTKodIme = %s AND VezTKod = %s"
        cursor.execute(query, (parent_table, table,))
        osnvkod = cursor.fetchone()

        
        
        
        
        query = "SELECT ime_tabele FROM metapodaci WHERE kodno_ime_tabele = %s"
        cursor.execute(query, (table,))
        datatable = None
        result = cursor.fetchone()
        if result is not None:
            datatable = database + "/" + result[0]

        if datatable not in tab_names:
            if item is not None:
                query = "SELECT {} from {} WHERE {}  = %s".format(osnvkod[0], parent_table, column_name)
                cursor.execute(query, (item,))
                x = cursor.fetchone()

                


                if column_name is not None:
                    query = "SELECT * FROM {} WHERE {} = %s".format(table, vezanekod[0])
                    cursor.execute(query, x)
                else:
                    query = "SELECT * FROM {}".format(table)
                    cursor.execute(query)
            else:
                query = "SELECT * FROM {}".format(table)
                cursor.execute(query)


            
            
            table_data = cursor.fetchall()
            column_names = [desc[0] for desc in cursor.description]


            
            cursor.close()
            connection.close()

            table_widget = QtWidgets.QTableWidget(self)
            table_widget.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
            data = table_data

            table_widget.setColumnCount(len(column_names))
            table_widget.setHorizontalHeaderLabels(column_names)
            table_widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

            table_widget.setRowCount(len(data))
            for row_index, row_data in enumerate(data):
                for col_index, cell_value in enumerate(row_data):
                    item = QtWidgets.QTableWidgetItem(str(cell_value))
                    table_widget.setItem(row_index, col_index, item)

            table_widget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            if datatable is not None:
                tab_widget.addTab(table_widget, datatable)
                tab_widget.setCurrentWidget(table_widget)


    def insertData(self):
        self.updated_items = []
        string = self.tab_widget1.tabText(self.tab_widget1.currentIndex())
        database, t = string.split('/')

        
        self.dialog = InsertDataDialog(database, self.table)
        if self.dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.refreshData(self.table, database, self.tab_widget1)
            print("Data insertion successful!")
        else:
            print("Data insertion canceled.")

    
    def deleteData(self):
        print("test")
        self.updated_items = []
        string = self.tab_widget1.tabText(self.tab_widget1.currentIndex())
        database, t = string.split('/')
        table = t.lower().replace(" ", "_")
        self.table_widget = self.tab_widget1.currentWidget()
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=self.database
        )
        cursor = conn.cursor()

        columns = self.get_column_names(table)

        selected_items = self.table_widget.selectedItems()

        if selected_items:
            row = selected_items[0].row()
            row_values = []
            for column in range(self.table_widget.columnCount()):
                item = self.table_widget.item(row, column)
                row_values.append(item.text())

            conditions = []
            for column_name in columns:
                conditions.append(f"{column_name} = %s")

            conditions_str = " AND ".join(conditions)
            query = f"DELETE FROM {self.table} WHERE {conditions_str}"
            cursor.execute(query, tuple(row_values))

        conn.commit()
        cursor.close()
        conn.close()
        self.refreshData(table, database, self.tab_widget1)




    def insertChildData(self):
        self.updated_items = []
        string = self.tab_widget2.tabText(self.tab_widget2.currentIndex())
        database, t = string.split('/')
        table = t.lower().replace(" ", "_")
        
        self.dialog = InsertDataDialog(database, table)
        if self.dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.refreshData(table, database, self.tab_widget2)
            print("Data insertion successful!")
        else:
            print("Data insertion canceled.")

    
    def deleteChildData(self):
        print("test")
        self.updated_items = []
        string = self.tab_widget2.tabText(self.tab_widget2.currentIndex())
        database, t = string.split('/')
        table = t.lower().replace(" ", "_")
        self.table_widget = self.tab_widget2.currentWidget()
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=self.database
        )
        cursor = conn.cursor()

        columns = self.get_column_names(table)

        selected_items = self.table_widget.selectedItems()

        if selected_items:
            row = selected_items[0].row()
            row_values = []
            for column in range(self.table_widget.columnCount()):
                item = self.table_widget.item(row, column)
                row_values.append(item.text())

            conditions = []
            for column_name in columns:
                conditions.append(f"{column_name} = %s")

            conditions_str = " AND ".join(conditions)
            query = f"DELETE FROM {table} WHERE {conditions_str}"
            cursor.execute(query, tuple(row_values))

        conn.commit()
        cursor.close()
        conn.close()
        self.refreshData(table, database, self.tab_widget2)

    


    def get_column_names(self, table):
        myconfig = config.load_config("myconfig.json")
        conn = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db=self.database
        )
        cursor = conn.cursor()

        query = f"SHOW COLUMNS FROM {table}"
        cursor.execute(query)

        column_names = [column[0] for column in cursor.fetchall()]

        cursor.close()
        conn.close()

        return column_names



    def refreshData(self, table, database, tab_widget):
        self.updated_items = []
        table_widget = tab_widget.currentWidget()

        if isinstance(table_widget, QtWidgets.QTableWidget):
            table_widget.clearContents()
            myconfig = config.load_config("myconfig.json")

            connection = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db=database
            )
            cursor = connection.cursor()
            query = "SELECT * FROM " + table
            cursor.execute(query)
            table_data = cursor.fetchall()
            column_names = [desc[0] for desc in cursor.description]
            cursor.close()
            connection.close()

            data = table_data

            table_widget.setColumnCount(len(column_names))
            table_widget.setHorizontalHeaderLabels(column_names)
            table_widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding) 

            table_widget.setRowCount(len(data))
            for row_index, row_data in enumerate(data):
                for col_index, cell_value in enumerate(row_data):
                    item = QtWidgets.QTableWidgetItem(str(cell_value))
                    table_widget.setItem(row_index, col_index, item)

            table_widget.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)

    def closeTab(self, index):
        self.updated_items = []
        self.tab_widget1.removeTab(index)



    def moveSelectionDown(self):
        table_widget = self.tab_widget1.currentWidget()
        selected_indexes = table_widget.selectedIndexes()
        
        if len(selected_indexes) == 0:
            table_widget.selectRow(0)
        else:
            current_row = selected_indexes[0].row()
            next_row = current_row + 1
            
            if next_row < table_widget.rowCount():
                table_widget.selectRow(next_row)

    
    def moveSelectionUp(self):
        table_widget = self.tab_widget1.currentWidget()
        selected_indexes = table_widget.selectedIndexes()
        
        if len(selected_indexes) == 0:
            table_widget.selectRow(0)
        else:
            current_row = selected_indexes[0].row()
            prev_row = current_row - 1
            

            table_widget.selectRow(prev_row)


    def moveSelectionBottom(self):
        table_widget = self.tab_widget1.currentWidget()
        
        
        last_row = table_widget.rowCount() - 1
            
        if last_row == table_widget.rowCount() - 1:
            table_widget.selectRow(last_row)

    
    def moveSelectionTop(self):
        table_widget = self.tab_widget1.currentWidget()
        
        table_widget.selectRow(0)





    
    def moveChildSelectionDown(self):
        table_widget = self.tab_widget2.currentWidget()
        selected_indexes = table_widget.selectedIndexes()
        
        if len(selected_indexes) == 0:
            table_widget.selectRow(0)
        else:
            current_row = selected_indexes[0].row()
            next_row = current_row + 1
            
            if next_row < table_widget.rowCount():
                table_widget.selectRow(next_row)

    
    def moveChildSelectionUp(self):
        table_widget = self.tab_widget2.currentWidget()
        selected_indexes = table_widget.selectedIndexes()
        
        if len(selected_indexes) == 0:
            table_widget.selectRow(0)
        else:
            current_row = selected_indexes[0].row()
            prev_row = current_row - 1
            

            table_widget.selectRow(prev_row)


    def moveChildSelectionBottom(self):
        table_widget = self.tab_widget2.currentWidget()
        
        
        last_row = table_widget.rowCount() - 1
            
        if last_row == table_widget.rowCount() - 1:
            table_widget.selectRow(last_row)

    
    def moveChildSelectionTop(self):
        table_widget = self.tab_widget2.currentWidget()
        
        table_widget.selectRow(0)


    def getColumnFromSelectedItem(self, table_widget):
        selected_items = table_widget.selectedItems()
        if len(selected_items) > 0:
            selected_item = selected_items[0]
            column_index = selected_item.column()
            column_header_item = table_widget.horizontalHeaderItem(column_index)
            if column_header_item is not None:
                column_name = column_header_item.text()
                return column_name
        return None



        
