import sys
from PySide2.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QComboBox, QPushButton, QCheckBox, QMessageBox
import mysql.connector
import json
import re
from PySide2.QtGui import QPalette, QColor
from PySide2.QtCore import Qt, QObject, Signal

class CreateTableWindowSignals(QObject):
    finished = Signal()
    
class TransparentLineEdit(QLineEdit):
    def __init__(self, placeholder_text):
        super().__init__()
        self.placeholder_text = placeholder_text
        self.setPlaceholderText(placeholder_text)

        palette = QPalette()
        palette.setColor(QPalette.Text, Qt.black)
        self.setPalette(palette)

        self.textChanged.connect(self.checkPlaceholderText)
        self.setFocusPolicy(Qt.StrongFocus)

    def checkPlaceholderText(self):
        if not self.text() and not self.hasFocus():
            self.setPlaceholderText(self.placeholder_text)

    def focusOutEvent(self, event):
        super().focusOutEvent(event)
        self.checkPlaceholderText()
            
class CreateTableWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Kreiraj tabelu")
        self.signals = CreateTableWindowSignals()
        self.setMinimumWidth(550)
        self.layout = QVBoxLayout()
        self.column_layouts = []
        self.foreign_key_layouts = []
        self.create_widgets()
        self.setLayout(self.layout)
        self.primary_key_columns = []

    def closeEvent(self, event):
        self.signals.finished.emit()
        super().closeEvent(event)
        
    def create_widgets(self):
        self.table_name_label = QLabel("Naziv tabele:")
        self.table_name_edit = TransparentLineEdit("Naziv tabele")
        self.layout.addWidget(self.table_name_label)
        self.layout.addWidget(self.table_name_edit)

        self.column_layout = QVBoxLayout()
        self.add_column_button = QPushButton("Dodaj kolonu")
        self.add_column_button.clicked.connect(self.add_column)
        self.column_layout.addWidget(self.add_column_button)
        self.layout.addLayout(self.column_layout)

        self.foreign_key_layout = QVBoxLayout()
        self.add_foreign_key_button = QPushButton("Dodaj strani kljuc")
        self.add_foreign_key_button.clicked.connect(self.add_foreign_key)
        self.foreign_key_layout.addWidget(self.add_foreign_key_button)
        self.layout.addLayout(self.foreign_key_layout)

        self.create_button = QPushButton("Kreiraj")
        self.create_button.clicked.connect(self.create_table)
        self.layout.addWidget(self.create_button)

    def add_column(self):
        column_layout = QHBoxLayout()
        column_name_edit = TransparentLineEdit("Naziv kolone")
        column_type_combo = QComboBox()
        column_type_combo.addItems(["INT", "VARCHAR(255)", "DATE", "FLOAT", "BOOLEAN"])
        primary_key_checkbox = QCheckBox("Primary Key")
        not_null_checkbox = QCheckBox("Not Null")
        column_layout.addWidget(column_name_edit)
        column_layout.addWidget(column_type_combo)
        column_layout.addWidget(primary_key_checkbox)
        column_layout.addWidget(not_null_checkbox)
        self.column_layouts.append((column_layout, primary_key_checkbox, not_null_checkbox))
        self.column_layout.insertLayout(self.column_layout.count() - 1, column_layout)

    def add_foreign_key(self):
        foreign_key_layout = QHBoxLayout()
        fk_column_name = TransparentLineEdit("Dodaj ogranicenja")
        fk_column_name.setMinimumWidth(200)
        foreign_key_table_combo = QComboBox()
        foreign_key_layout.addWidget(fk_column_name)
        foreign_key_layout.addWidget(foreign_key_table_combo)
        self.foreign_key_layouts.append((foreign_key_layout, foreign_key_table_combo))
        self.foreign_key_layout.insertLayout(self.foreign_key_layout.count() - 1, foreign_key_layout)

        self.populate_table_dropdown(foreign_key_table_combo)

    def populate_table_dropdown(self, foreign_key_table_combo):
        try:
            myconfig = self.load_config("myconfig.json")
            cnx = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db="polyinfo"
            )

            cursor = cnx.cursor()
            cursor.execute("""
                SELECT TABLE_NAME, COLUMN_NAME
                FROM information_schema.KEY_COLUMN_USAGE
                WHERE CONSTRAINT_NAME = 'PRIMARY'
                AND TABLE_SCHEMA = %s
            """, ("polyinfo",))

            result = {}
            for table_name, column_name in cursor:
                table_name = table_name.upper()
                if table_name not in result:
                    result[table_name] = []
                result[table_name].append(column_name)
                
            table_strings = []
            for table_name, primary_key_columns in result.items():
                pk_string = ','.join(primary_key_columns)
                table_string = f"{table_name} ({pk_string})"
                table_strings.append(table_string)

            for table in table_strings:
                foreign_key_table_combo.addItem(table)

            cnx.close()
        except mysql.connector.Error as error:
            QMessageBox.critical(self, "Error", str(error))

    def create_table(self):
        table_name_from_text_box = self.table_name_edit.text()
        if not table_name_from_text_box:
            QMessageBox.warning(self, "Error", "Unesite naziv tabele.")
            return

        if len(self.column_layouts) == 0:
            QMessageBox.warning(self, "Error", "Dodajte bar jednu kolonu.")
            return

        table_name = table_name_from_text_box.strip()
        table_name = self.replace_invalid_characters_from_table_name(table_name).upper()
        create_table_query = f"CREATE TABLE {table_name} ("

        self.primary_key_columns = []
        new_table_fk_keys_column_name = []
        new_table_fk_keys = []
        for layout, foreign_key_table_combo in self.foreign_key_layouts:
            fk_column_name = layout.itemAt(0).widget().text().upper()
            if fk_column_name in new_table_fk_keys_column_name:
                QMessageBox.warning(self, "Error", "Ne možete koristiti isto ime stranog ključa više puta.")
                return
            foreign_key_table = foreign_key_table_combo.currentText()
            if foreign_key_table in new_table_fk_keys:
                QMessageBox.warning(self, "Error", "Ne možete koristiti istu tabelu za strani ključ više puta.")
                return
            new_table_fk_keys_column_name.append(fk_column_name)
            new_table_fk_keys.append(foreign_key_table)
            fk_values = foreign_key_table.split(' ')
            table_name_reference = fk_values[0]
            create_table_query += self.get_primary_keys(table_name_reference, fk_column_name)
            
        regular_columns = []
        for layout, primary_key_checkbox, _ in self.column_layouts:
            column_name_label = layout.itemAt(0).widget().text().strip()
            column_name = self.replace_invalid_characters_from_table_name(column_name_label).upper()
            column_type = layout.itemAt(1).widget().currentText()
            is_primary_key = primary_key_checkbox.isChecked()
            not_null_checkbox = layout.itemAt(3).widget()
            is_not_null = not_null_checkbox.isChecked()

            create_table_query += f"{column_name} {column_type}"

            if is_primary_key:
                self.primary_key_columns.append({"column_name": column_name, "column_name_label": column_name_label})
            else:
                regular_columns.append({"column_name": column_name, "column_name_label": column_name_label})

            if is_not_null:
                create_table_query += " NOT NULL"

            create_table_query += ", "
            
        if self.primary_key_columns:
            primary_columns = [item['column_name'] for item in self.primary_key_columns]
            create_table_query += f"PRIMARY KEY ({', '.join(primary_columns)}), "
        else:
            QMessageBox.warning(self, "Error", "Dodajte najmanje jednu kolonu primarnog ključa.")
            return

        create_table_query = create_table_query.rstrip(", ") + ")"

        try:
            myconfig = self.load_config("myconfig.json")
            cnx = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db="polyinfo"
            )

            cursor = cnx.cursor()
            cursor.execute(create_table_query)

            vezane_tabele_data = []
            
            for layout, foreign_key_table_combo in self.foreign_key_layouts:
                fk_column_name = layout.itemAt(0).widget().text().upper()
                foreign_key_table = foreign_key_table_combo.currentText()
                if foreign_key_table:
                    fk_values = foreign_key_table.split(' ')
                    foreign_keys_from_new_table = fk_values[1].replace("(", "(" + fk_column_name + "_").replace(",", "," + fk_column_name + "_")
                    alter_table_query = f"ALTER TABLE {table_name} ADD CONSTRAINT FK_{fk_column_name} FOREIGN KEY {foreign_keys_from_new_table} REFERENCES {foreign_key_table} ON DELETE RESTRICT ON UPDATE RESTRICT;"
                    cursor.execute(alter_table_query)
                    vezane_tabele_data_entity = {
                        "OsnITabele": table_name_from_text_box,
                        "OsnTKodIme": table_name,
                        "VezTIme": fk_values[0],
                        "VezTKod": fk_values[0],
                        "OsnvKod": foreign_keys_from_new_table,
                        "PkOsn": 1,
                        "VezaneKod": fk_values[1],
                        "VezanePK": 1
                    }
                    vezane_tabele_data.append(vezane_tabele_data_entity)

            cnx.commit()
            cnx.close()

            QMessageBox.information(self, "Success", "Tabela je uspesno kreirana.")
            self.close()
            self.add_new_table_in_metadata_table(self.primary_key_columns, regular_columns, table_name_from_text_box, table_name)
            self.add_new_table_in_vezanetabele_table(vezane_tabele_data)
        except mysql.connector.Error as error:
            cursor = cnx.cursor()
            drop_table_query = f"DROP TABLE IF EXISTS {table_name}"
            cursor.execute(drop_table_query)
            cnx.close()
            QMessageBox.critical(self, "Error", str(error))

    def add_new_table_in_metadata_table(self, primary_keys_column, regular_columns, table_name, code_table_name):
        for primary_key in primary_keys_column:
            data = [table_name, code_table_name, primary_key["column_name_label"], primary_key["column_name"], True]
            self.insert_data_into_metadata_table(data)
            
        for regular_column in regular_columns:
            data = [table_name, code_table_name, regular_column["column_name_label"], regular_column["column_name"], False]
            self.insert_data_into_metadata_table(data)
        
    def insert_data_into_metadata_table(self, data):
        try:
            myconfig = self.load_config("myconfig.json")
            cnx = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db="polyinfo"
            )

            cursor = cnx.cursor()
            
            table_name = "metapodaci"
            column_names = ["ime_tabele", "kodno_ime_tabele", "labela", "kod", "pk", "id"]
            
            select_query = f"SELECT MAX(Id) FROM {table_name}"
            cursor.execute(select_query)
            result = cursor.fetchone()
            latest_id = result[0]
            data.append(latest_id + 1)

            insert_query = f"INSERT INTO {table_name} ({', '.join(column_names)}) VALUES "

            value_placeholders = ", ".join(["%s"] * len(column_names))

            insert_query += f"({value_placeholders})"

            cursor.execute(insert_query, data)

            cnx.commit()
            cnx.close()

        except mysql.connector.Error as error:
            cnx.close()
            print(f"Greska prilikom upisa entiteta u tabelu 'metapodaci': {error}")
        
    def add_new_table_in_vezanetabele_table(self, vezane_tabele_data):
        for vezane_tabele_data_entity in vezane_tabele_data:
            foreign_key_columns_new_table = vezane_tabele_data_entity['OsnvKod'].replace("(", "").replace(")", "").split(",") 
            foreign_key_columns_referenced_table = vezane_tabele_data_entity['VezaneKod'].replace("(", "").replace(")", "").split(",") 
            counter = 0
            for foreign_key_column in foreign_key_columns_new_table:
                vezaneKod = foreign_key_columns_referenced_table[counter]
                data = [vezane_tabele_data_entity["OsnITabele"], vezane_tabele_data_entity["OsnTKodIme"], 
                        vezane_tabele_data_entity["VezTIme"], vezane_tabele_data_entity["VezTKod"], 
                        foreign_key_column, vezane_tabele_data_entity["PkOsn"], 
                        vezaneKod,vezane_tabele_data_entity["VezanePK"]]
                counter += 1
                self.insert_data_into_vezanetabele_table(data)
                
    def insert_data_into_vezanetabele_table(self, data):
        try:
            myconfig = self.load_config("myconfig.json")
            cnx = mysql.connector.connect(
                host=myconfig["host"],
                user=myconfig["user"],
                password=myconfig["password"],
                db="polyinfo"
            )

            cursor = cnx.cursor()
            
            table_name = "vezanetabele"
            column_names = ["OsnITabele", "OsnTKodIme", "VezTIme", "VezTKod", "OsnvKod", "PkOsn", "VezaneKod", "VezanePK", "id"]
            
            select_query = f"SELECT MAX(Id) FROM {table_name}"
            cursor.execute(select_query)
            result = cursor.fetchone()
            latest_id = result[0]
            data.append(latest_id + 1)

            insert_query = f"INSERT INTO {table_name} ({', '.join(column_names)}) VALUES "

            value_placeholders = ", ".join(["%s"] * len(column_names))

            insert_query += f"({value_placeholders})"

            cursor.execute(insert_query, data)

            cnx.commit()
            cnx.close()

        except mysql.connector.Error as error:
            cnx.close()
            print(f"Greska prilikom upisa entiteta u tabelu 'vezanetabele': {error}")
        
    def load_config(self, path):
        with open(path, "r", encoding="utf-8") as f:
            return json.load(f)
        
    def replace_invalid_characters_from_table_name(self, table_name):
        pattern = r"[^\w\d]+"
        table_name = re.sub(pattern, "_", table_name)
        return table_name.strip('_')
    
    def get_primary_keys(self, table_name, fk_column_name):
        myconfig = self.load_config("myconfig.json")
        cnx = mysql.connector.connect(
            host=myconfig["host"],
            user=myconfig["user"],
            password=myconfig["password"],
            db="polyinfo"
        )

        cursor = cnx.cursor()
        cursor.execute(f"SHOW COLUMNS FROM {table_name} WHERE `Key` = 'PRI'")

        primary_keys = []
        for column in cursor:
            column_name = column[0]
            column_type = column[1].decode()
            column_null = "not null" if column[2] == "NO" else "null"
            primary_keys.append(f"{fk_column_name}_{column_name} {column_type} {column_null},")
            self.primary_key_columns.append({"column_name": f"{fk_column_name}_{column_name}", "column_name_label": f"{fk_column_name}_{column_name}"})

        cnx.close()

        return " ".join(primary_keys)


def main():
    app = QApplication(sys.argv)
    window = CreateTableWindow()
    window.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
